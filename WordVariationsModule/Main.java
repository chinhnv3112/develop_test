package WordVariationsModule;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean pointer = true;
        Scanner scanner = new Scanner(System.in);
        while (pointer) {
            System.out.println("\n Input your text (ENTER if you have close apllication): ");
            String inputWordVariations = scanner.nextLine();
            if (inputWordVariations.equalsIgnoreCase("")) {
                pointer = false;
                break;
            }
            WordVariations wordVariations = new WordVariations(inputWordVariations);
            wordVariations.findWord();
        }

        scanner.close();
    }
}
