package WordVariationsModule;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class WordVariations {
    private String input;

    public WordVariations(String input) {
        super();
        this.input = input;
    }

    public void findWord() {
        List<String> matchedWords = findWordsWithNumbersAndLatinCharacters(this.input);
        System.out.println("Matched Words: " + matchedWords);

        List<String> variations = generateVariations(matchedWords);
        System.out.println("Variations with '-' character: " + variations);
    }


    public static List<String> findWordsWithNumbersAndLatinCharacters(String input) {
        List<String> matchedWords = new ArrayList<>();
        String[] words = input.split(" ");

        for (String word : words) {
            if (containsNumbersAndLatinCharacters(word)) {
                matchedWords.add(word);
            }
        }

        return matchedWords;
    }

    public static boolean containsNumbersAndLatinCharacters(String word) {
        boolean hasNumber = false;
        boolean hasLatinCharacters = false;

        for (char c : word.toCharArray()) {
            if (Character.isDigit(c)) {
                hasNumber = true;
            } else if (Character.isLetter(c)) {
                hasLatinCharacters = true;
            }
        }

        return hasNumber && hasLatinCharacters;
    }

    public static List<String> generateVariations(List<String> words) {
        List<String> variations = new ArrayList<>();

        for (String word : words) {
            List<String> wordSplit = Arrays.asList(word.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)"));
            List<String> results = addHyphenBetweenCharacterAndNumber(wordSplit, wordSplit);
            variations.addAll(results);
        }

        return variations;
    }

    public static List<String> addHyphenBetweenCharacterAndNumber(List<String> array, List<String> wordSplit) {

        List<String> arrayResult = new ArrayList<>();
        
        if (array.size() == 1) {
            arrayResult.add(array.get(0));
            return arrayResult;
        }

        for (int index = 0; index < array.size(); index++) {
            StringBuilder textBefore = new StringBuilder();
            for (int i = 0; i <= index; i++) {
                textBefore.append(array.get(i));
            }

            List<String> subArray = array.subList(index + 1, array.size());
            List<String> resultSubArray = addHyphenBetweenCharacterAndNumber(subArray, wordSplit);
            for (String result : resultSubArray) {
                arrayResult.add(textBefore + "-" + result);
            }
        }

        if (!array.isEmpty() && array.size() != wordSplit.size()) {
            arrayResult.add(String.join("", array));
        }

        return arrayResult;
    }

}
